import React, { act } from 'react';
import { render, screen } from '@testing-library/react';
import App from './App';

test('renders API URL', async () => {
  await act(async () => {
    render(<App />);
  });
  const apiUrlElement = await screen.findByText(/API URL/i);
  expect(apiUrlElement).toBeInTheDocument();
});
