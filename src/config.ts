const getEnvVariable = (key: string): string | undefined => {
  return process.env[key];
};

const env = process.env.REACT_APP_ENV || process.env.NODE_ENV;

const apiUrl =
  env === 'local'
    ? getEnvVariable('REACT_APP_API_URL_LOCAL')
    : env === 'development'
      ? getEnvVariable('REACT_APP_API_URL_DEVELOPMENT')
      : env === 'test'
        ? getEnvVariable('REACT_APP_API_URL_TEST')
        : env === 'production'
          ? getEnvVariable('REACT_APP_API_URL_PRODUCTION')
          : '';

export { apiUrl };
