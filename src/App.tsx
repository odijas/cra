import { apiUrl } from './config';

import './App.css';

function App() {
  return (
    <div className="App">
      <div>
        <h1>API URL: {apiUrl}</h1>
      </div>
    </div>
  );
}

export default App;
